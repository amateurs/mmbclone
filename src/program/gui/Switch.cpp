#include "gui.h"

namespace mmb {

    Switch::Switch(std::string lButton, std::string rButton, sf::Vector2f size, sf::Vector2f pos) {
        left = new Button(pos, lButton, size.y*3);
        right = new Button(sf::Vector2f(pos.x+size.x+size.y+11, pos.y), rButton, size.y*3);
        text = new Text("", sf::Vector2f(pos.x+size.y+15, pos.y), size.y*3);

        shape.setSize(size);
        shape.setPosition(sf::Vector2f(pos.x+size.y+9, pos.y+1));
        shape.setFillColor(sf::Color(128, 128, 128, 128));
        shape.setOutlineColor(sf::Color(108, 108, 108));
        shape.setOutlineThickness(1);

        counter = 0;
    }

    Switch::~Switch() {
        delete left;
        delete right;
        delete text;
    }

    void Switch::setPosition(sf::Vector2f pos) {
        left->setPosition(pos);
        shape.setPosition(sf::Vector2f(pos.x+shape.getSize().y+5, pos.y));
        text->setPosition(shape.getPosition());
        right->setPosition(sf::Vector2f(pos.x+shape.getSize().x+5, pos.y));
    }

    void Switch::setSize(sf::Vector2f s) {
        shape.setSize(s);
        right->setPosition(sf::Vector2f(s.x+s.x+s.y+11, s.y));
    }

    void Switch::buttonEvents(sf::RenderWindow& rw) {
        if (left->containsMouseCursor(rw) && sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
            if (counter>0) counter--;
            else counter = options.size()-1;
            text->setString(options.at(counter));
            text->setCharacterSize(shape.getSize().y*3);
        } else if (right->containsMouseCursor(rw) && sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
            if (counter<options.size()-1) counter++;
            else counter = 0;
            text->setString(options.at(counter));
            text->setCharacterSize(shape.getSize().y*3);
        }
    }

    void Switch::addOption(std::string opt) {
        options.push_back(opt);
        if (options.size()==1) {
            text->setString(options.at(counter));
            text->setCharacterSize(shape.getSize().y*3);
        }
    }

    void Switch::setCurrentOption(std::string o) {
        int i=0;
        for (auto x : options) {
            if (x==o) break;
            i++;
        }
        counter = i;
        text->setString(options.at(counter));
        text->setCharacterSize(shape.getSize().y*3);
    }

    std::string Switch::getCurrentOption() {
        return options.at(counter);
    }

    void Switch::draw(sf::RenderWindow& rw) {
        rw.draw(shape);
        left->draw(rw);
        right->draw(rw);
        text->draw(rw);
    }

}
