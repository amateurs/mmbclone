#ifndef funcs_h
#define funcs_h

namespace mmb {

    template<class Base, class T>
    inline bool instanceof(const T* ptr) {
        return dynamic_cast<const Base*>(ptr) != nullptr;
    }

}

#endif // funcs_h
