#ifndef item_h
#define item_h

#include <SFML/Graphics.hpp>

namespace mmb {

    class Item {
    protected:
        int amount;
        double ID;
        std::string name;
        std::string description;
        std::string effects;
        std::string requirements;
        sf::FloatRect boundBox;
    public:
        virtual ~Item() = default;
        virtual int getAmount() = 0;
        virtual short getBonus() = 0;
        virtual double getID() = 0;
        virtual bool isDisposable() = 0;
        virtual std::string getName() = 0;
        virtual std::string getDescription() = 0;
        virtual sf::Texture getSkin() = 0;
        virtual sf::VertexArray getBody() = 0;
        virtual void draw(sf::RenderWindow&) = 0;
        virtual void editAmount(int) = 0;
        virtual void update() = 0;
    };

    class Potion :public Item {
    private:
        short bonus;

        sf::Texture skin;
        sf::VertexArray body;
    public:
        Potion(double ID, std::string itemName, short icon, int am, short val, sf::Vector2f pos = sf::Vector2f(0, 0));
        ~Potion();

        virtual int getAmount();
        virtual short getBonus();
        virtual double getID();
        virtual bool isDisposable();
        virtual std::string getName();
        virtual std::string getDescription();
        virtual sf::Texture getSkin();
        virtual sf::VertexArray getBody();
        virtual void draw(sf::RenderWindow&);
        virtual void editAmount(int);
        virtual void update();
    };

}

#endif
