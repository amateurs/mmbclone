#include "menus.h"

namespace mmb {

    PauseMenu::PauseMenu(sf::RenderWindow& rw) {
        title = new Text("MMB CLONE", 40, sf::Color::Green);
        title->setPosition(sf::Vector2f(rw.getView().getSize().x/2-title->getSize().x/2, 0));

        wMenu = new Window("", sf::Vector2f(78, 112.5), sf::Vector2f(5, rw.getView().getSize().y/2-57.5));
            wMenu->addComponent(new Button(sf::Vector2f(wMenu->getPosition().x+5, wMenu->getPosition().y+5), "RESUME", 30));
            wMenu->addComponent(new Button(sf::Vector2f(wMenu->getPosition().x+5, wMenu->getPosition().y+23), "SAVE", 30));
            wMenu->addComponent(new Button(sf::Vector2f(wMenu->getPosition().x+5, wMenu->getPosition().y+41), "LOAD", 30));
            wMenu->addComponent(new Button(sf::Vector2f(wMenu->getPosition().x+5, wMenu->getPosition().y+59), "OPTIONS", 30));
            wMenu->addComponent(new Button(sf::Vector2f(wMenu->getPosition().x+5, wMenu->getPosition().y+77), "HELP", 30));
            wMenu->addComponent(new Button(sf::Vector2f(wMenu->getPosition().x+5, wMenu->getPosition().y+95), "EXIT", 30));
        wMenu->setVisible(true);

        wOpts = new Window("OPTIONS", sf::Vector2f(78, 112.5), sf::Vector2f(rw.getView().getSize().x-83, rw.getView().getSize().y/2-57.5));
            wOpts->addComponent(new Text("THERE'S NOTHING YET.", sf::Vector2f(wOpts->getPosition().x+4, wOpts->getPosition().y+15), 15));
        wOpts->addComponent(new Button(sf::Vector2f(wOpts->getPosition().x+5, wOpts->getPosition().y+95), "QUIT", 30));
            wOpts->setVisible(false);


        wHelp = new Window("HELP", sf::Vector2f(78, 112.5), sf::Vector2f(rw.getView().getSize().x-83, rw.getView().getSize().y/2-57.5));
            wHelp->addComponent(new Text("Protip:", sf::Vector2f(wHelp->getPosition().x+4, wHelp->getPosition().y+15), 15, sf::Color::Yellow));
            wHelp->addComponent(new Text("Kill yourself.", sf::Vector2f(wHelp->getPosition().x+4, wHelp->getPosition().y+20), 15, sf::Color::Red));
        wHelp->addComponent(new Button(sf::Vector2f(wHelp->getPosition().x+5, wHelp->getPosition().y+95), "QUIT", 30));
            wHelp->setVisible(false);
    }

    PauseMenu::~PauseMenu() {
        delete wMenu;
        delete wOpts;
        delete wHelp;
    }

    void PauseMenu::buttonEvents(sf::RenderWindow& rw, Game* g) {
        if (wMenu->isVisible() && !wOpts->isVisible() && !wHelp->isVisible()) {
            if (wMenu->getComponent<Button>(0)->containsMouseCursor(rw) && sf::Mouse::isButtonPressed(sf::Mouse::Left)) g->pause(false);
            if (wMenu->getComponent<Button>(1)->containsMouseCursor(rw) && sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
                g->save();
                g->pause(false);
            }
            if (wMenu->getComponent<Button>(2)->containsMouseCursor(rw) && sf::Mouse::isButtonPressed(sf::Mouse::Left)) if (g->load()) g->pause(false);
            if (wMenu->getComponent<Button>(3)->containsMouseCursor(rw) && sf::Mouse::isButtonPressed(sf::Mouse::Left)) wOpts->setVisible(true);
            if (wMenu->getComponent<Button>(4)->containsMouseCursor(rw) && sf::Mouse::isButtonPressed(sf::Mouse::Left)) wHelp->setVisible(true);
            if (wMenu->getComponent<Button>(5)->containsMouseCursor(rw) && sf::Mouse::isButtonPressed(sf::Mouse::Left)) g->start(false);
        }
        else if (wOpts->isVisible()) {
            if (wOpts->getComponent<Button>(0)->containsMouseCursor(rw) && sf::Mouse::isButtonPressed(sf::Mouse::Left)) wOpts->setVisible(false);
        }
        else if (wHelp->isVisible()) {
            if (wHelp->getComponent<Button>(0)->containsMouseCursor(rw) && sf::Mouse::isButtonPressed(sf::Mouse::Left)) wHelp->setVisible(false);
        }
    }

    void PauseMenu::draw(sf::RenderWindow& rw, sf::View& v) {
        rw.setView(*new sf::View(sf::Vector2f(142, 80), sf::Vector2f(284, 160)));
        title->draw(rw);
        wMenu->draw(rw);
        wOpts->draw(rw);
        wHelp->draw(rw);
        rw.setView(v);
    }
}
