#include "menus.h"

namespace mmb {

    MainMenu::MainMenu(sf::RenderWindow& rw) {
        title = new Text("MMB CLONE", 40, sf::Color::Green);
        title->setPosition(sf::Vector2f(rw.getView().getSize().x/2-title->getSize().x/2, 0));

        wMenu = new Window("", sf::Vector2f(78, 112.5), sf::Vector2f(5, rw.getView().getSize().y/2-57.5));
            wMenu->addComponent(new Button(sf::Vector2f(wMenu->getPosition().x+5, wMenu->getPosition().y+5), "NEW GAME", 30));
            wMenu->addComponent(new Button(sf::Vector2f(wMenu->getPosition().x+5, wMenu->getPosition().y+23), "LOAD", 30));
            wMenu->addComponent(new Button(sf::Vector2f(wMenu->getPosition().x+5, wMenu->getPosition().y+41), "OPTIONS", 30));
            wMenu->addComponent(new Button(sf::Vector2f(wMenu->getPosition().x+5, wMenu->getPosition().y+59), "HELP", 30));
            wMenu->addComponent(new Button(sf::Vector2f(wMenu->getPosition().x+5, wMenu->getPosition().y+77), "CREDITS", 30));
            wMenu->addComponent(new Button(sf::Vector2f(wMenu->getPosition().x+5, wMenu->getPosition().y+95), "EXIT", 30));
        wMenu->setVisible(true);

        wOpts = new Window("OPTIONS", sf::Vector2f(78, 112.5), sf::Vector2f(rw.getView().getSize().x-83, rw.getView().getSize().y/2-57.5));
            wOpts->addComponent(new Text("RESOLUTION:", sf::Vector2f(wOpts->getPosition().x+4, wOpts->getPosition().y+15), 15));
            wOpts->addComponent(new Switch("<", ">", sf::Vector2f(40, 5), sf::Vector2f(wOpts->getPosition().x+4, wOpts->getPosition().y+25)));
                wOpts->getComponent<Switch>(0)->addOption("800x420");
                wOpts->getComponent<Switch>(0)->addOption("1280x720");
                wOpts->getComponent<Switch>(0)->addOption("1440x900");
                wOpts->getComponent<Switch>(0)->addOption("1600x900");
                wOpts->getComponent<Switch>(0)->addOption("1920x1080");
            wOpts->addComponent(new Checkbox(sf::Vector2f(wOpts->getPosition().x+4, wOpts->getPosition().y+45), "FULLSCREEN", 15));
            wOpts->addComponent(new Checkbox(sf::Vector2f(wOpts->getPosition().x+4, wOpts->getPosition().y+55), "VSYNC", 15));
        wOpts->addComponent(new Button(sf::Vector2f(wOpts->getPosition().x+5, wOpts->getPosition().y+95), "QUIT", 30));
            wOpts->setVisible(false);


        wHelp = new Window("HELP", sf::Vector2f(78, 112.5), sf::Vector2f(rw.getView().getSize().x-83, rw.getView().getSize().y/2-57.5));
            wHelp->addComponent(new Text("Protip:", sf::Vector2f(wHelp->getPosition().x+4, wHelp->getPosition().y+15), 15, sf::Color::Yellow));
            wHelp->addComponent(new Text("Kill yourself.", sf::Vector2f(wHelp->getPosition().x+4, wHelp->getPosition().y+20), 15, sf::Color::Red));
        wHelp->addComponent(new Button(sf::Vector2f(wHelp->getPosition().x+5, wHelp->getPosition().y+95), "QUIT", 30));
            wHelp->setVisible(false);

        wCred = new Window("CREDITS", sf::Vector2f(78, 112.5), sf::Vector2f(rw.getView().getSize().x-83, rw.getView().getSize().y/2-57.5));
            wCred->addComponent(new Text("PROGRAMMING:", sf::Vector2f(wCred->getPosition().x+4, wCred->getPosition().y+15), 15));
            wCred->addComponent(new Text(L"\tAdam 'Adanos' Gąsior", sf::Vector2f(wCred->getPosition().x+4, wCred->getPosition().y+20), 15, sf::Color::Yellow));
            wCred->addComponent(new Text("GRAPHICS:", sf::Vector2f(wCred->getPosition().x+4, wCred->getPosition().y+30), 15));
            wCred->addComponent(new Text(L"\tKamil 'Kwasior' Kwaśnik", sf::Vector2f(wCred->getPosition().x+4, wCred->getPosition().y+35), 15, sf::Color::Yellow));
            wCred->addComponent(new Text("MUSIC:", sf::Vector2f(wCred->getPosition().x+4, wCred->getPosition().y+45), 15));
            wCred->addComponent(new Text(L"\tAdam 'Adanos' Gąsior", sf::Vector2f(wCred->getPosition().x+4, wCred->getPosition().y+50), 15, sf::Color::Yellow));
            wCred->addComponent(new Text("\tKacper 'Kacepru'", sf::Vector2f(wCred->getPosition().x+4, wCred->getPosition().y+55), 15, sf::Color::Yellow));
            wCred->addComponent(new Text("\tMarek Czyszy", sf::Vector2f(wCred->getPosition().x+4, wCred->getPosition().y+60), 15, sf::Color::Yellow));
        wCred->addComponent(new Button(sf::Vector2f(wCred->getPosition().x+5, wCred->getPosition().y+95), "QUIT", 30));
            wCred->setVisible(false);

    }

    MainMenu::~MainMenu() {
        delete wMenu;
        delete wOpts;
        delete wHelp;
        delete wCred;
    }

    void MainMenu::buttonEvents(sf::RenderWindow& rw, Game* g) {
        if (wMenu->isVisible() && !wOpts->isVisible() && !wHelp->isVisible() && !wCred->isVisible()) {
            if (wMenu->getComponent<Button>(0)->containsMouseCursor(rw) && sf::Mouse::isButtonPressed(sf::Mouse::Left)) if (g->loadNewGame()) g->start(true);
            if (wMenu->getComponent<Button>(1)->containsMouseCursor(rw) && sf::Mouse::isButtonPressed(sf::Mouse::Left)) if (g->load()) g->start(true);
            if (wMenu->getComponent<Button>(2)->containsMouseCursor(rw) && sf::Mouse::isButtonPressed(sf::Mouse::Left)) wOpts->setVisible(true);
            if (wMenu->getComponent<Button>(3)->containsMouseCursor(rw) && sf::Mouse::isButtonPressed(sf::Mouse::Left)) wHelp->setVisible(true);
            if (wMenu->getComponent<Button>(4)->containsMouseCursor(rw) && sf::Mouse::isButtonPressed(sf::Mouse::Left)) wCred->setVisible(true);
            if (wMenu->getComponent<Button>(5)->containsMouseCursor(rw) && sf::Mouse::isButtonPressed(sf::Mouse::Left)) rw.close();
        }
        else if (wOpts->isVisible()) {
            wOpts->getComponent<Switch>(0)->buttonEvents(rw);
            if (wOpts->getComponent<Button>(0)->containsMouseCursor(rw) && sf::Mouse::isButtonPressed(sf::Mouse::Left)) wOpts->setVisible(false);
            for (unsigned i=0; i<3; i++)
                if (wOpts->getComponent<Button>(i)->containsMouseCursor(rw) && sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
                    if (!wOpts->getComponent<Checkbox>(i)->isChecked()) wOpts->getComponent<Checkbox>(i)->check(true);
                    else wOpts->getComponent<Checkbox>(i)->check(false);
                }
        }
        else if (wHelp->isVisible()) {
            if (wHelp->getComponent<Button>(0)->containsMouseCursor(rw) && sf::Mouse::isButtonPressed(sf::Mouse::Left)) wHelp->setVisible(false);
        }
        else if (wCred->isVisible()) {
            if (wCred->getComponent<Button>(0)->containsMouseCursor(rw) && sf::Mouse::isButtonPressed(sf::Mouse::Left)) wCred->setVisible(false);
        }
    }

    void MainMenu::draw(sf::RenderWindow& rw, sf::View& v) {
        rw.setView(*new sf::View(sf::Vector2f(142, 80), sf::Vector2f(284, 160)));
        title->draw(rw);
        wMenu->draw(rw);
        wOpts->draw(rw);
        wHelp->draw(rw);
        wCred->draw(rw);
        rw.setView(v);
    }
}
