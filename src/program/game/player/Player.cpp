#include "player.h"

namespace mmb {

    Player::Player(sf::Vector2f pos) {
        skin.loadFromFile("data/graphics/BaseHooman.png");

        body.resize(4);
        body.setPrimitiveType(sf::Quads);

        body[0].position = pos;
        body[1].position = sf::Vector2f(pos.x+200, pos.y);
        body[2].position = sf::Vector2f(pos.x+200, pos.y+200);
        body[3].position = sf::Vector2f(pos.x, pos.y+200);

        body[0].texCoords = sf::Vector2f(0, 0);
        body[1].texCoords = sf::Vector2f(200, 0);
        body[2].texCoords = sf::Vector2f(200, 200);
        body[3].texCoords = sf::Vector2f(0, 200);

        position = pos;
        velocity = 0.125;
        hp = 30;
        sp = 0;
        exp = 0;
        lvl = 0;
    }

    Player::~Player() {

    }

    void Player::setPosition(sf::Vector2f pos) {
        if (position.x != pos.x || position.y != pos.y) position = pos;
        body[0].position = pos;
        body[1].position = sf::Vector2f(pos.x+200, pos.y);
        body[2].position = sf::Vector2f(pos.x+200, pos.y+200);
        body[3].position = sf::Vector2f(pos.x, pos.y+200);
    }

    void Player::go(double ts, std::string di) {
        if (di == "up") {
            position.y -= ts*velocity;
            setPosition(position);
        }
        else if (di == "down") {
            position.y += ts*velocity;
            setPosition(position);
        }
        else if (di == "left") {
            position.x -= ts*velocity;
            setPosition(position);
        }
        else if (di == "right") {
            position.x += ts*velocity;
            setPosition(position);
        }
    }

    void Player::draw(sf::RenderWindow& rw) {
        rw.draw(body, &skin);
    }

}
