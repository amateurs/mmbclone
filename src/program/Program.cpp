#include "program.h"
#include "funcs/files.h"
#include <cstdio>
#include <ctime>

namespace mmb {

    Program::Program(char* arg) {
        if (readConfig()) {
            if (arg == (char*)"play") runGame();
            else if (arg == (char*)"edit") runEditor();
            else runGame();
        }
    }

    Program::Program() {
        if (readConfig()) {
            game = new Game(window);
            runGame();
        }
    }

    Program::~Program() {
        delete game;
    }

    void Program::runGame() {
        view.setSize(sf::Vector2f(284, 160));
        view.setCenter(sf::Vector2f(142, 80));
        window.create(sf::VideoMode(resolution.x, resolution.y, 32), "MMBClone", fullscreen ? sf::Style::Fullscreen : sf::Style::Default);
        window.setVerticalSyncEnabled(vsync);
        window.setView(view);

        sf::Clock timer;
        game = new Game(window);
        while (window.isOpen()) {
            while (window.pollEvent(event)) {
                if (event.type == sf::Event::Closed) window.close();
                if (event.type == sf::Event::Resized) view.setSize((sf::Vector2f)window.getSize());
                game->buttonEvents(window, view);
            }
            game->controls(timer.getElapsedTime().asMicroseconds());

            window.clear();
            game->draw(window, view);
            window.display();
            timer.restart();
        }
    }

    void Program::runEditor() {
        window.create(sf::VideoMode(resolution.x, resolution.y, 32), "MMBClone - Story Editor", fullscreen ? sf::Style::Fullscreen : sf::Style::Default);
        window.setVerticalSyncEnabled(vsync);
        view.setSize((sf::Vector2f)window.getSize());

        while (window.isOpen()) {
            while (window.pollEvent(event)) {
                if (event.type == sf::Event::Closed) window.close();
                if (event.type == sf::Event::Resized) view.setSize((sf::Vector2f)window.getSize());
            }
            window.clear();
            window.display();
        }
    }

    bool Program::readConfig() {
        std::ifstream iconfig;
        try {
            iconfig.open("config.cfg");
            if (!iconfig.good()) throw "File not found";
            puts("Loading a config file...");

            while (!iconfig.eof()) {
                std::string param;
                iconfig >> param;

                if (param[0] == ';') {
                    std::getline(iconfig, param);
                    continue;
                } else if (param == "") continue;
                else if (param == "width:") readFile(iconfig, resolution.x);
                else if (param == "height:") readFile(iconfig, resolution.y);
                else if (param == "fullscreen:") readFile(iconfig, fullscreen);
                else if (param == "vsync:") readFile(iconfig, vsync);
                else throw "Wrong parameter";
            }
        } catch (...) {
            puts("Error loading config.cfg!");

            iconfig.clear();
            iconfig.sync();

            puts("Creating a new config file...");
            std::ofstream oconfig("config.cfg");
            oconfig.clear();
            oconfig << ";--------------;\n";
            oconfig << ";video settings;\n";
            oconfig << ";----screen----;\n";
            oconfig << "width:\t\t1280\n";
            oconfig << "height:\t\t720\n";
            oconfig << "fullscreen:\t0\n";
            oconfig << "vsync:\t\t1\n";
            oconfig << ";---graphics---;\n";
            oconfig << ";--------------;";

            oconfig.close();
            readConfig();
        }
        iconfig.close();
        return true;
    }

}
