#ifndef program_h
#define program_h

#include <SFML/Graphics.hpp>
#include <fstream>
#include "game/game.h"

namespace mmb {

    class Program {
    private:
        Game* game;

        sf::RenderWindow window;
        sf::Event event;
        sf::View view;
        sf::Vector2u resolution;

        bool vsync;
        bool fullscreen;
    public:
        Program(char*);
        Program();
        ~Program();

        void runGame();
        void runEditor();
        bool readConfig();
    };

}

#endif // program_h
