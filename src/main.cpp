#include "program/program.h"
#include <cstdlib>
#include <ctime>

int main(int argc, char** argv) {
    srand(time(0));
    mmb::Program* p;

    if (argc == 1) p = new mmb::Program();
    else p = new mmb::Program(argv[1]);

    delete p;
    return 0;
}
