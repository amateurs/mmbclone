w folderze src/ jest plik main.cpp
i folder program/, w którym jest nagłówek program.hpp i plik program.cpp
w tych plikach jest deklaracja i definicja klasy Program, która służy do zarządzania całym programem (pętla główna, ustawienia, obsługa wydarzeń, etc.)
w klasie Program jest obiekt klasy Game, która jest zadeklarowana i zdefiniowana w folderze src/program/game/
i w tej klasie są wszystkie rzeczy dotyczące gameplayu, tzn. postać gracza, poziomy, status (rozpoczęta lub nie, spauzowana lub nie), etc.
GUI trzymam w src/program/gui/, gdzie wszystkie klasy są w jednym nagłówku
wszystkie funkcje dotyczące niskopoziomowych rzeczy są w src/program/funcs/ (obsługa tablic wierzchołków, funkcje matematyczne i statystyczne, etc.)
wszystkie klasy dotyczące tylko gameplayu trzymam w src/program/game/nazwakomponentu/, na przykład plik player.hpp jest w src/program/game/player/
albo dla lepszego przyporządkowania: src/program/game/entity/player/player.hpp - klasa Player dziedziczy z klasy Entity w src/program/game/entity/entity.hpp
strukturę plików już ci w miarę opisałem, całość jest dość intuicyjna
jeszcze tylko importowanie nagłówków ze wcześniejszych folderów:
jak plik jest np. w src/program/game/, a chcesz w nim zaincludować coś z src/program/funcs/, to musisz w tym pliku napisać: #include "../funcs/cośtam.hpp"
"../" oznacza przejście do poprzedniego folderu
i to wszystko w sumie
aaa, jeszcze jedna ważna rzecz:
w każdym nagłówku MUSISZ zrobić tak:

#ifndef nagłówek_hpp
#define nagłówek_hpp

// tu tworzysz klasy

#endif
jeśli tego nie zrobisz, to powstanie konflikt definicji, jeżeli będziesz próbował zaimportować jeden nagłówek więcej niż raz